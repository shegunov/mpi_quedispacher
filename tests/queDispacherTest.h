#include<cassert>
#include<iostream>
#include<mpi.h>

#include "queDispacher.h"

using QueDispacher = QueDispacher;
using Task = QueDispacher::Task;


struct RankAccumulator : public Task
{
public:
    RankAccumulator(QueDispacher &dispacher, bool verbose);
    void onBeforeRun() override;
    void toExecute() override;
    void onPostRun() override;
    ~RankAccumulator() override;
private:
    void dumpToConsole();
    int globalRank;
    int taskCnt;
    bool verbose;
};

inline RankAccumulator::RankAccumulator(QueDispacher& dispacher, bool verbose)
    :Task(dispacher), globalRank(-1), taskCnt(0), verbose(verbose)
{

}

inline void RankAccumulator::onBeforeRun()
{
    int rank = 0;
    MPI_Comm_rank(globalComm, &rank);
    globalRank = rank;
}

inline void RankAccumulator::onPostRun()
{
    int totalTasksCnt = 0;
    MPI_Allreduce(&taskCnt, &totalTasksCnt, 1, MPI_INT, MPI_SUM, globalComm);
    int rank = 0;
    MPI_Comm_rank(globalComm, &rank);
    if (rank == 0 && verbose) {
        std::cout << "\n\n\n TotalTastCNT: " << totalTasksCnt << "\n" << std::flush;
    }
    MPI_Barrier(globalComm);

    MPI_Status s;
    int flag;
    MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, globalComm, &flag, &s);
    if(flag != 0 && verbose) {
        std::cout << "rank: " << rank << " from: " << s.MPI_SOURCE << " tag: " << s.MPI_TAG;
        exit(1);
    }

    assert(totalTasksCnt == int(dispacher.getTotalTasksCnt()));
}

inline void RankAccumulator::toExecute()
{
    int rank = 0;
    MPI_Comm_rank(localComm, &rank);
    if (rank == 0) {
        taskCnt++;
        dumpToConsole();
    }

    //MPI_Bcast(&taskCnt, 1, MPI_INT, 0, localComm);
}

inline void RankAccumulator::dumpToConsole()
{
    int rank = 0;
    MPI_Comm_rank(localComm, &rank);
    if (rank == 0 && verbose) {
        std::cout << "Grb: " << globalRank / dispacher.getGroupSize() << " processing Task...\n" << std::flush;
    }
}

inline RankAccumulator::~RankAccumulator()
{

}
 
