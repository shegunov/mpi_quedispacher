#include "queDispacherTest.h"
#include<unistd.h>

bool randomAllocTest(int tasks, int grb, int chunk, bool verbose = false)
{
    int rank, sz;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &sz);


    MPI_Barrier(MPI_COMM_WORLD);
    std::cout << std::flush;

    bool skip = false;
    if (rank == 0 && verbose) {
        if(grb >= sz) {
            std::cout << "Skiping Test:" " - Not enough resources" "\n";
            skip = true;
        }
        std::cout << "Performing Test\n";
        std::cout << "-----------------------------------\n\n";
    }

    MPI_Bcast(&skip, 1, MPI_CXX_BOOL, 0, MPI_COMM_WORLD);

    if (skip)
        return true;

    QueDispacher simpleDispacher(MPI_COMM_WORLD, tasks, grb, chunk);
    RankAccumulator task(simpleDispacher, verbose);
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
    std::cout << std::flush;
    if (rank == 0 && verbose) {
        std::cout << "\nTest Passed: " << __FUNCTION__  << "\n\n";
    }
    return true;
}


void singleMessage(bool verbose)
{
    int rank, sz;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &sz);
    randomAllocTest(sz - 1, 1 , 1, verbose);
}

void multipleMessages(bool verbose)
{
    randomAllocTest(200, 1 , 1, verbose);
}

void largeChunkBlocks(bool verbose)
{
    randomAllocTest(10, 1 , 2, verbose);
}

void grbAllocationDefaultChunck(bool verbose)
{
    randomAllocTest(10, 2 , 1, verbose);
}

void grbAllocationDefaultLargeChunk(bool verbose)
{
    randomAllocTest(10, 2 , 2, verbose);
}

bool performTest(void (*test) (bool verbose), const char* name = "", bool verbose = true)
{
    int rank = 0;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Barrier(MPI_COMM_WORLD);
    std::cout << std::flush;
    if (rank == 0 && verbose) {
        std::cout << "Performing Test:" << name << "\n";
        std::cout << "-----------------------------------\n\n";
    }
    test(verbose);
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
    std::cout << std::flush;
    if (rank == 0 && verbose) {
        std::cout << "\nTest Passed: " << name << "\n\n";
    }
    return true;
}

void iterativeTest(bool verbose)
{
    int rank, sz;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &sz);

    if (rank == 0 && verbose) {
        std::cout << "Performing Test:" << __PRETTY_FUNCTION__ << "\n";
        std::cout << "-----------------------------------\n\n";
    }

    for (int i = 100; i < 200; i+=50)
        for (int j = 1; j < 20; j++) {
            if (sz <= j)
                break;

            for(int k = 1; k <= i; k++)
                randomAllocTest(i, j, k, false);

        }

    if (rank == 0 && verbose) {
        std::cout << "Test passed:\n";
        std::cout << "-----------------------------------\n\n";
    }
}

int main (int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int rank, sz;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &sz);
    if (sz == 1) {
        std::cout << "Test must be run with more then one processor\n";
        MPI_Finalize();
        return 0;
    }

    performTest(singleMessage, "singleMessage");
    performTest(multipleMessages, "multipleMessages");
    performTest(largeChunkBlocks, "largeChunkBlocks");
    performTest(grbAllocationDefaultChunck, "grbAllocation");
    performTest(grbAllocationDefaultLargeChunk, "grbAllocationDefaultLargeChunk");
    performTest(iterativeTest, "Extensive test");
    MPI_Finalize();
    return 0;
}
