cmake_minimum_required(VERSION 3.1)
set(CMAKE_COLOR_MAKEFILE ON)

if(CMAKE_SOURCE_DIR STREQUAL CMAKE_BUILD_DIR)
   message(ERROR "You don't want to configure in the source directory!")
endif()

set (CMAKE_CXX_STANDARD 14)
project(queDispacher VERSION 1)
set(CMAKE_CXX_STANDARD_REQUIRED True)

#Set flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -g3 -Wall -Wpedantic")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3 -DNDDEBUG")

#Functional Tests
enable_testing()

## Aditional optional Components options#####################
#############################################################

set(LIBS "")
## 3-th party libs #########################################
find_package(MPI REQUIRED)
if (MPI_FOUND)
  include_directories(${MPI_INCLUDE_PATH})
  set(LIBS "${LIBS} ${MPI_LIBRARIES}")
endif()
#############################################################

##Config File################################################
configure_file (
  "${CMAKE_CURRENT_SOURCE_DIR}/queDispacherConfig.h.in"
  "${PROJECT_BINARY_DIR}/queDispacherConfig.h"
  )
#############################################################

include_directories("${CMAKE_CURRENT_BINARY_DIR}")

include_directories("${CMAKE_CURRENT_SOURCE_DIR}/src")
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/src")

include_directories("${CMAKE_CURRENT_SOURCE_DIR}/tests")
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/tests")
