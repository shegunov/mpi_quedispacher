#include<mpi.h>

#ifndef QUE_DISP_H
#define QUE_DISP_H

class QueDispacher
{
public:
    struct Task
    {
        friend QueDispacher;

    public:
        Task(const QueDispacher& dispacher);
        void operator() ();

        virtual void onBeforeRun() = 0;
        virtual void toExecute() = 0;
        virtual void onPostRun() = 0;

        virtual ~Task() = 0;

    protected:
        const QueDispacher& dispacher;
        const MPI_Comm& localComm;
        const MPI_Comm& globalComm;
    };

    QueDispacher(MPI_Comm comm, unsigned taskCnt, unsigned grbSz = 1, unsigned chunkSz = 1);

    unsigned getTotalTasksCnt() const;
    unsigned getChunkSize() const;
    unsigned getGroupSize() const;

    ~QueDispacher();

private:
    void run(Task& toExec) const;

    enum {MASTER_RANK = 0};
    enum {UNASSIGNED_PROCESSORS = MPI_UNDEFINED};
    enum {HAS_WORK = 1, NEED_WORK = 2, STOP = 0};

    void setPrcColor();
    void makeComs();

    void stopComputation() const;
    void dispachWork(int& nTasks) const;
    void worker(Task& toCall) const;

    int procColor;
    MPI_Comm localComm;
    MPI_Comm globalComm;

    const int taskCnt;
    const unsigned grbSz;
    const unsigned chunkSz;
    unsigned groupCount;
};

inline QueDispacher::Task::Task(const QueDispacher& dispacher)
    :dispacher(dispacher), localComm(dispacher.localComm), globalComm(dispacher.globalComm)
{ }

inline void QueDispacher::Task::operator() ()
{
    dispacher.run(*this);
}

inline  QueDispacher::Task::~Task()
{

}

inline unsigned QueDispacher::getChunkSize() const
{
    return chunkSz;
}

inline unsigned QueDispacher::getGroupSize() const
{
    return grbSz;
}

inline unsigned QueDispacher::getTotalTasksCnt() const
{
    return taskCnt;
}

inline QueDispacher::~QueDispacher()
{
    if (localComm != MPI_COMM_NULL) {
         MPI_Comm_free(&localComm);
    }
}
 
#endif // QUE_DISP_H
