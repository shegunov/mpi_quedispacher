#include <cassert>
#include "queDispacher.h"


QueDispacher::QueDispacher(MPI_Comm comm, unsigned taskCnt, unsigned grbSz, unsigned chunkSz)
    :globalComm(comm), taskCnt(taskCnt), grbSz(grbSz), chunkSz(chunkSz), groupCount(0)
{
    assert(taskCnt > 0);
    procColor = UNASSIGNED_PROCESSORS;
    localComm = MPI_COMM_NULL;
    setPrcColor();
    makeComs();
}

void QueDispacher::setPrcColor()
{
    int rank, commSz;
    MPI_Comm_rank(globalComm, &rank);
    MPI_Comm_size(globalComm, &commSz);

    assert(unsigned(commSz - 1) >= grbSz);
    groupCount = (commSz - 1) / grbSz;

    procColor = UNASSIGNED_PROCESSORS;

    if (rank == 0)
        procColor = rank;
    else if (unsigned(rank - 1) < groupCount * grbSz)
            procColor = 1 + (rank - 1) / grbSz;
}

void QueDispacher::makeComs()
{
    int rank;
    MPI_Comm_rank(globalComm, &rank);
    MPI_Comm_split(globalComm, procColor, rank, &localComm);
}

void QueDispacher::run(Task& toCall) const
{
    MPI_Barrier(globalComm);

    int rank, commSz;
    MPI_Comm_rank(globalComm, &rank);
    MPI_Comm_size(globalComm, &commSz);
    assert(commSz > 1);

    toCall.onBeforeRun();

    int nTasks = taskCnt;
    if (procColor != UNASSIGNED_PROCESSORS) {
        if (rank == MASTER_RANK) {
            while(true) {
                if (nTasks == 0) {
                    stopComputation();
                    break;
                } else {
                    dispachWork(nTasks);
                }

            }
        } else {
            worker(toCall);
        }
    }

    MPI_Barrier(globalComm);
    toCall.onPostRun();
    MPI_Barrier(globalComm);
}

void QueDispacher::stopComputation() const
{


    unsigned i = 0;
    int rank = 1;
    do {
        MPI_Recv(nullptr, 0, MPI_INT, rank, NEED_WORK, globalComm, MPI_STATUS_IGNORE);
        MPI_Send(nullptr, 0, MPI_INT, rank, STOP, globalComm);
        i++;
        rank += grbSz;
    } while (i < groupCount);

}

void QueDispacher::dispachWork(int& nTasks) const
{

    int hasMsg = 0;
    int localWorkSz = 0;

    unsigned i = 0;
    int rank = 1;
    do {

        //If group i needs work send msg
        MPI_Iprobe(rank, NEED_WORK, globalComm, &hasMsg, MPI_STATUS_IGNORE);
        if (hasMsg) {
            MPI_Recv(nullptr, 0, MPI_INT, rank, NEED_WORK, globalComm, MPI_STATUSES_IGNORE);

            localWorkSz = nTasks - int(chunkSz) < 0 ? nTasks : chunkSz;
            nTasks -= localWorkSz;
            assert(nTasks >= 0);
            MPI_Send(&localWorkSz, 1, MPI_INT, rank, HAS_WORK, globalComm);
        }
        i++;
        rank += grbSz;
    } while (i < groupCount);
}

void QueDispacher::worker(Task& toCall) const
{
    int rankGl, commSzGl;
    MPI_Comm_rank(globalComm, &rankGl);
    MPI_Comm_size(globalComm, &commSzGl);

    int rankLoc, commSzLoc;
    MPI_Comm_rank(localComm, &rankLoc);
    MPI_Comm_size(localComm, &commSzLoc);

    int nSamples = 0;
    bool running = true;
    while(true) {
        if (rankLoc == 0) {
            MPI_Status status;
            MPI_Send(nullptr, 0, MPI_INT, MASTER_RANK , NEED_WORK, globalComm);
            MPI_Probe(MASTER_RANK, MPI_ANY_TAG, globalComm,  &status);
            if (status.MPI_TAG == STOP) {
                MPI_Recv(nullptr, 0, MPI_INT, MASTER_RANK, STOP, globalComm, MPI_STATUS_IGNORE);
                running = false;
            }
            else if(status.MPI_SOURCE == MASTER_RANK) {
                MPI_Recv(&nSamples, 1, MPI_INT, MASTER_RANK, HAS_WORK, globalComm, MPI_STATUS_IGNORE);
            }
            else {
                std::runtime_error("Unknown message recived");
            }
        }

        int pack[2] = {nSamples, running};
        MPI_Bcast(&pack, 2, MPI_INT, 0, localComm);
        nSamples = pack[0];
        running = pack[1];
        if (!running) {
            break;
        }

        for (int i = 0; i < nSamples; i++) {
            toCall.toExecute();
        }
    }
}
