# MPI_queDispacher

MPI_queDispacher is a small C++ library that implements a simple abstract Master-Slave parallel que, using MPI.

## Installation

### Build from source

git clone https://bitbucket.org/shegunov/MPI_queDispacher


mkdir build


cd build


cmake ../


make


make install



Please note that this library depends on MPI. CMake provides a way to find the MPI library.
If your installation is not in the default path, please refer to CMake documentation, for more information on how to set MPI Lib


## Usage
Please see tests for examples on how to use this library

## Contributing

## License
[MIT](https://choosealicense.com/licenses/mit/)

